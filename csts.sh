#!/bin/bash
#CSTS.sh
#CANT STOP THE SIGNAL
# version v0.3

#Bash Script for endocing an archive and attaching it to a png image
#requires p7zip-full, imagemagick
#
#For v.1 you need
#    1) payload directory for files to attach
#    2) display.png for "body" message image

archiveFile=archive.7z
payloadDir="payload"
rm $archiveFile
7z -t7z -mx=9 a ${archiveFile} ${payloadDir}


width=`identify -format "%w" display.png`
height=`identify -format "%h" display.png`
archiveSize=`wc -c $archiveFile | cut -d ' ' -f 1`
padding="( ( $width * 3 ) - ( $archiveSize % ( $width * 3 ) ) )"

padding=`echo -e "$padding" | bc`


truncate -s +${padding} $archiveFile

dataHeight=$(( ( archiveSize +  padding  ) / ( width * 3  ) ))
echo padding is $padding , height is $dataHeight, archiveSize is $archiveSize
convert -size ${width}x${dataHeight} -depth 8 rgb:$archiveFile archiveImg.png

waterMark="Compiled with CSTS - Can't Stop The Signal - gitlab.com/OpitcalReverb/CSTS"

#add directions
directions="1. Right click -> save this image
2. Download and install gimp
www.gimp.org/downloads/
3. Open the saved image file in gimp
3. Click Image -> Canvas size
4. In the Set Image Canvas Size window set
width: ${width}, height: ${dataHeight}
offset X: 0, offset Y: -${height}
5. Click Image -> Flatten Image
6. Click File -> Export As
7. Save the image as archive.data and type raw format
8. Download and install 7zip
www.7-zip.org
9. Open archive.data with 7zip"

convert display.png \
  -gravity north -pointsize 30 \
  -stroke '#000C' -strokewidth 2 -annotate 0 "${waterMark}" \
  -stroke  none   -fill white    -annotate 0 "${waterMark}" \
  -gravity west -pointsize 24 \
  -stroke '#000C' -strokewidth 2 -annotate 0 "${directions}" \
  -stroke  none   -fill white    -annotate 0 "${directions}" \
  displayTemp.png

convert displayTemp.png archiveImg.png -append publish.png

echo "cleaning temporary files"
rm displayTemp.png archiveImg.png
echo "your file is ready!"

#todo 
# 1) command line options and flags
# 2) optional encryption/padding to obfuscate against file signature matching